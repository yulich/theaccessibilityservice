package com.jeff.theaccessibilityservice.service

import android.accessibilityservice.AccessibilityService
import android.content.Intent
import android.view.accessibility.AccessibilityEvent
import com.jeff.theaccessibilityservice.script.ScriptAlipay
import com.jeff.theaccessibilityservice.script.ScriptBase
import com.jeff.theaccessibilityservice.script.ScriptSwiftApp
import com.log.JFLog


// AccessibilityService:
// https://www.jianshu.com/p/7ffaf7ca07bc
// https://juejin.im/entry/587d8da9b123db4d5e7e49a2
// https://www.jianshu.com/p/405f74ed000b
// https://www.jianshu.com/p/4cd8c109cdfb
// https://www.cnblogs.com/lanxingren/p/9983904.html
// https://juejin.im/post/584bd285a22b9d0058d7713e
// https://blog.csdn.net/angcyo/article/details/51100898
// https://github.com/YanChenYuan/AccessibilityServiceTest

// 獲取佈局:
// https://developer.android.com/studio/debug/layout-inspector?hl=zh-cn

class MyAccessibility : AccessibilityService() {

    companion object {
        var accessibilityService: MyAccessibility? = null
    }

    override fun onServiceConnected() {
        super.onServiceConnected()

        JFLog.d("onServiceConnected")

        accessibilityService = this

        /*
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        */
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        // rootInActiveWindow 取得窗口
        // JFLog.d("$rootInActiveWindow")

        JFLog.d("onAccessibilityEvent: ${event.toString()}")

        if (event != null) {
            if (AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED == event.eventType) {
                val rootNode = this.rootInActiveWindow
                if (rootNode != null) {
                    JFLog.i("Root package id: ${rootNode.packageName}")

                    when (rootNode.packageName) {
                        "com.jeff.swiftapp" -> ScriptSwiftApp.executeRoot(rootNode)
                        "io.busniess.va.sandhook" -> ScriptAlipay.executeRoot(rootNode)
                        // else ->  ScriptBase.executeRoot(rootNode)
                    }
                }
            }
        }
    }

    override fun onUnbind(intent: Intent?): Boolean {
        // 關閉服務時呼叫
        JFLog.d("onUnbind")

        accessibilityService = null

        return super.onUnbind(intent)
    }

    override fun onInterrupt() {
        // 當服務被中斷時被呼叫
        JFLog.d("onInterrupt")

        accessibilityService = null
    }
}