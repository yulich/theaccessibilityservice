package com.jeff.theaccessibilityservice

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.jeff.theaccessibilityservice.script.AutoScript
import com.jeff.theaccessibilityservice.script.ScriptThis
import com.jeff.theaccessibilityservice.service.MyAccessibility
import com.log.JFLog

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        JFLog.setGlobalMaxLineLength(220)

        if (isAccessibilityServiceEnabled(this)) {
            openAppWithPackageName(this, "com.jeff.swiftapp")
            // openAppWithPackageName(this, "com.maobc.customer")
        } else {
            val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
            startActivity(intent)
        }

        // 透過 rootInActiveWindow
        findViewById<Button>(R.id.btn1).setOnClickListener {
            val service = MyAccessibility.accessibilityService
            if (service != null) {
                ScriptThis.executeRoot(service.rootInActiveWindow)
            }

            AutoScript.actionRecent()
        }
    }
}
