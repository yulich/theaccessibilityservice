package com.jeff.theaccessibilityservice

import android.accessibilityservice.AccessibilityServiceInfo
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.view.accessibility.AccessibilityManager

fun openAppWithPackageName(app: Activity, pkgName: String): Boolean {
    val pkgManager = app.packageManager

    val packageInfo: PackageInfo

    try {
        packageInfo = pkgManager.getPackageInfo(pkgName, 0)
    } catch (e: PackageManager.NameNotFoundException) {
        return false
    }

    val resolveIntent = Intent(Intent.ACTION_MAIN, null)
    resolveIntent.setPackage(packageInfo.packageName)

    val apps = pkgManager.queryIntentActivities(resolveIntent, 0)
    val ri = apps.iterator().next()
    if (ri != null) {
        val findPkgName = ri.activityInfo.packageName
        val className = ri.activityInfo.name

        val intent = Intent(Intent.ACTION_MAIN)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED

        // 重點
        intent.component = ComponentName(findPkgName, className)
        app.startActivity(intent)

        return true
    }

    return false
}

fun isAccessibilityServiceEnabled(app: Activity): Boolean {
    val manager = app.getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager

    val enableServices = manager.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_ALL_MASK)

    for (i in 0 until enableServices.size) {
        if ("com.jeff.theaccessibilityservice/.service.MyAccessibility" == enableServices[i].id) {
            return true
        }
    }

    return false
}