package com.jeff.theaccessibilityservice.script

import android.view.accessibility.AccessibilityNodeInfo
import com.log.JFLog

class ScriptSwiftApp {

    companion object {
        fun executeRoot(info: AccessibilityNodeInfo) {
            // JFLog.i("Node: $info")
            JFLog.i("Node className: ${info.className}")
            JFLog.i("Node id: ${info.viewIdResourceName}")

            if (info.childCount == 0) {
                if (info.text != null) {
                    JFLog.i("Node text: ${info.text}")
                    executeAction(info)
                }
            } else {
                JFLog.i("Node child count: ${info.childCount}")
                for (i in 0 until info.childCount) {
                    executeRoot(info.getChild(i))
                }
            }
        }

        private fun executeAction(info: AccessibilityNodeInfo) {
            if ("FastTitleBar" == info.text) {
                if (info.isClickable) {
                    info.performAction(AccessibilityNodeInfo.ACTION_CLICK)

                    /* No work
                    val bundle = Bundle()
                    bundle.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "Jeff")
                    info.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, bundle)
                    */
                }
            }
        }
    }
}