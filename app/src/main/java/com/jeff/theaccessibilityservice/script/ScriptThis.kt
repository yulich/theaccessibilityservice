package com.jeff.theaccessibilityservice.script

import android.os.Bundle
import android.view.accessibility.AccessibilityNodeInfo
import com.log.JFLog

class ScriptThis {

    companion object {
        fun executeRoot(info: AccessibilityNodeInfo) {
            // JFLog.i("Node: $info")
            JFLog.i("Node className: ${info.className}")
            JFLog.i("Node id: ${info.viewIdResourceName}")

            if (info.childCount == 0) {
                if (info.text != null) {
                    JFLog.i("Node text: ${info.text}")
                }

                if (info.viewIdResourceName != null) {
                    executeAction(info)
                }
            } else {
                JFLog.i("Node child count: ${info.childCount}")
                for (i in 0 until info.childCount) {
                    executeRoot(info.getChild(i))
                }
            }
        }

        private fun executeAction(info: AccessibilityNodeInfo) {
            if (info.viewIdResourceName.endsWith("edit1")) {
                val bundle = Bundle()
                bundle.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, info.viewIdResourceName)
                info.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, bundle)
            }
        }
    }
}