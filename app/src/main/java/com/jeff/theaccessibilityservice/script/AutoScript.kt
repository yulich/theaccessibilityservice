package com.jeff.theaccessibilityservice.script

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.GestureDescription
import android.graphics.Path
import com.jeff.theaccessibilityservice.service.MyAccessibility

class AutoScript {

    companion object {

        fun actionRecent() {
            MyAccessibility.accessibilityService?.performGlobalAction(AccessibilityService.GLOBAL_ACTION_RECENTS)
        }

        fun close() {

            val path = Path()
            path.moveTo(100F,100F)

            val description
                    =  GestureDescription.Builder()
                    .addStroke(GestureDescription.StrokeDescription(path, 100, 50))
                    .build()

            // MyAccessibility.accessibilityService?.dispatchGesture(description, )
        }
    }
}