package com.jeff.theaccessibilityservice.script

import android.view.accessibility.AccessibilityNodeInfo
import com.log.JFLog

class ScriptBase {

    companion object {
        fun executeRoot(info: AccessibilityNodeInfo) {
            // JFLog.i("Node: $info")
            JFLog.i("Node className: ${info.className}")
            JFLog.i("Node id: ${info.viewIdResourceName}")

            if (info.childCount == 0) {
                if (info.text != null) {
                    JFLog.i("Node text: ${info.text}")

                }

                if (info.isClickable) {
                    executeAction(info)
                }
            } else {
                JFLog.i("Node child count: ${info.childCount}")
                for (i in 0 until info.childCount) {
                    executeRoot(info.getChild(i))
                }
            }
        }

        private fun executeAction(info: AccessibilityNodeInfo) {

        }
    }
}